import knex from 'knex'

const config: knex.Config = {
  client: 'sqlite3',
  connection: {
    filename: 'db.sqlite'
  },
  useNullAsDefault: true
}

export default config
