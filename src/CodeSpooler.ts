import path from 'path'
import { OutlookService } from './OutlookService'
import { EmailModel } from './EmailModel'
import { readFileSync, writeFileSync } from 'fs'
import Knex from 'knex'
import chalk from 'chalk'
import { Email } from './Email'
import { EMPTY_SPOOL, EMPTY_QUEUE, SEND_ERROR, MISSING_CODE, PERSISTING_ERROR, NO_CHANGE } from './ErrorCodes'

export class CodeSpooler {
  private source: string
  private db: Knex
  private mailer: OutlookService

  constructor(db: Knex, mailer: OutlookService) {
    this.source = path.join(process.cwd(), 'spool', process.env.CODE_SPOOL || 'codes')
    this.db = db
    this.mailer = mailer
  }

  public async process() {
    console.log(chalk.green('Spooling Codes'))
    const codes: string[] = readFileSync(this.source)
      .toString()
      .split('\n')
      .filter(e => e.length > 0)

    if (codes.length === 0) {
      console.warn(chalk.yellow(`${EMPTY_SPOOL}: No codes to spool`))
    }

    const emailsToSend: EmailModel[] = await this.db<EmailModel>('entries')
      .where('sent', false)

    if (emailsToSend.length === 0) {
      console.warn(chalk.yellow(`${EMPTY_QUEUE}: No emails to send`))
    }     

    let currentEntry: EmailModel | undefined
    let currentCode: string | undefined

    // Loop only runs if we have emails to process
    while(currentEntry = emailsToSend.pop()) {
      let changed: boolean = false

      // Check to see if the entry has a code or not and assign it 
      // a code from the spool if it doesn't have one
      if (!currentEntry.code && (currentCode = codes.pop())) {
        currentEntry.code = currentCode
        changed = true
      }

      // Attempt to send the email. If its successful we set the 
      // Sent status to true
      if (!!currentEntry.code) {
        try { 
          await this.mailer.send(new Email('Online Textbook', 'download_code', currentEntry))
          currentEntry.sent = true
          changed = true
        } catch(err) {
          console.error(chalk.red(`${SEND_ERROR}: Error sending mail to ${currentEntry.email}\n\t${err}`))
        }
      } else {
        console.warn(chalk.yellow(`${MISSING_CODE}: Warning ${currentEntry.email} has no code`))
      }
     
      // Save the entry only if we updated the entry 
      if (changed) {
        try {
          await this.db<EmailModel>('entries')
            .where('id', currentEntry.id)
            .update(currentEntry)
        } catch(err) {
          console.error(chalk.yellow(`${PERSISTING_ERROR}: ${currentEntry.email} -> ${currentEntry.code}\n\t${err}`))
        }
      } else {
        console.warn(chalk.yellow(`${NO_CHANGE}: Not updating ${currentEntry.email} in DB`))
      }
    }

    // With any codes left over write them to the spool
    writeFileSync(this.source, codes.join('\n'))
  }
}