import path from 'path'
import { readFileSync, writeFileSync } from 'fs'
import Knex from 'knex'
import chalk from 'chalk'

import { EmailModel } from './EmailModel'
import { EMPTY_SPOOL, PERSISTING_ERROR, DUPLICATE } from './ErrorCodes'

export class EmailSpooler {
  private source: string
  private db: Knex

  constructor(db: Knex) {
    this.source = path.join(process.cwd(), 'spool', process.env.EMAIL_SPOOL || 'emails')
    this.db = db
  }

  public async process() {
    console.log(chalk.green('Spooling Emails'))
    const data: string[] = readFileSync(this.source)
      .toString()
      .split('\n')
      .filter(e => e.length > 0)

    if (data.length === 0) {
      console.warn(chalk.yellow(`${EMPTY_SPOOL}: No emails to spool`))
    }

    // Keep track of DB errors (except duplicate emails)
    // We want to try these emails again at a later point
    const failedEmails: string[] = []

    let currentEmail: string | undefined
    while (currentEmail = data.pop()) {
      // Check if it exists in the database
      let savedEntry = await this.db<EmailModel>('entries')
        .where('email', currentEmail).first()

      // Add it to the database if it doesn't exist
      if (!savedEntry) {
        try {
          await this.db<EmailModel>('entries').insert({ email: currentEmail, sent: false, code: '' })
        } catch(err) {
          // If there was a failure add it to failed list to be saved later
          console.error(chalk.red(`${PERSISTING_ERROR}: ${err}`))
          failedEmails.push(currentEmail)
        }
      } else {
        // Warn and move on
        console.warn(chalk.yellow(`${DUPLICATE}: email ${currentEmail} already exits`))
      }
    }
       
    // Update list
    writeFileSync(this.source, failedEmails.join('\n'))
  }
}