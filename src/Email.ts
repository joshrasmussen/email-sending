import { EmailModel } from "./EmailModel"
import Mustache from 'mustache'
import path from 'path'
import { readFileSync } from "fs"

export class Email {
  readonly subject: string
  private template: string
  private model: EmailModel
  
  constructor(subject: string, template: string, model: EmailModel) {
    this.subject = subject
    this.template = readFileSync(path.join(process.cwd(), 'templates', `${template}.html`)).toString()
    this.model = model
  }

  get html(): string {
    return Mustache.render(this.template, this.model)
  }

  get to(): string {
    return this.model.email
  }
}