export interface EmailModel {
  id: number
  email: string
  code: string
  sent: boolean
}
