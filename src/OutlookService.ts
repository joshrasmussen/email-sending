import nodemailer from 'nodemailer'
import { Email } from './Email'
import { User } from './User'
import Mail from 'nodemailer/lib/mailer'

export class OutlookService {
  readonly transport: Mail
  private user: User

  constructor(user: User) {
    this.user = user

    this.transport = nodemailer.createTransport({
      host: 'smtp-mail.outlook.com',
      secure: false,
      port: 587,
      tls: {
        ciphers: 'SSLv3',
      },
      auth: {
        user: user.email,
        pass: user.pass,
      },
    })
  }

  public async send(email: Email) {
    const mailOptions: Mail.Options = {
      from: `${this.user.name} <${this.user.email}>`,
      to: email.to,
      subject: email.subject,
      html: email.html,
    }

    return new Promise((resolve, reject) => {
      this.transport.sendMail(mailOptions, (err, info) => {
        if (err) {
          reject(err)
        } else {
          resolve(info)
        }
      })
    })
  }
}