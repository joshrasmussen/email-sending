import dotenv from 'dotenv'
import chalk from 'chalk'
import { User } from './src/User'
import { EmailSpooler } from './src/EmailSpooler'
import { CodeSpooler } from './src/CodeSpooler'
import dbconfig from './src/database'
import Knex from 'knex'
import { OutlookService } from './src/OutlookService'

dotenv.config()

function loadUser(): User {
  if (!process.env.OUTLOOK_EMAIL || !process.env.OUTLOOK_PASSWORD) {
    console.error("Must provide Outlook credentials")
    process.exit(1)
  }

  return {
    name: process.env.OUTLOOK_NAME || 'Sent from Typescript',
    email: process.env.OUTLOOK_EMAIL,
    pass: process.env.OUTLOOK_PASSWORD 
  }
}

async function main() {
  const db = Knex(dbconfig)

  console.log(chalk.cyan('Migrating database'))
  await db.migrate.latest()
  
  const outlook = new OutlookService(loadUser())

  const espool = new EmailSpooler(db)
  await espool.process()

  const cspool = new CodeSpooler(db, outlook) 
  await cspool.process()

  db.destroy()
}

main()
