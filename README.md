# Usage

Create a directory called `spool` that has two files. 

- One called `emails` which is a list of emails separated by a newline that is expected to have an email sent. 
- One called `codes` which is a newline separated list of codes that should be allocated to each email. 

You also need to create a `.env` file that has the environment variables as follows:

- OUTLOOK_EMAIL 
- OUTLOOK_PASSWORD
- OUTLOOK_NAME
- CODE_SPOOL
- EMAIL_SPOOL

To run this program you just run the following commands:

```bash
yarn install
yarn start

// OR

npm install
npm run start
```

## Features

By running this script a few things happen. The files in the `spool` directory 
are loaded and processed. Any lines that are processed are loaded into a **sqlite**
database allowing you to rerun the script to try again. Lines that are processed in
the spool are removed. **files in the spools are mutable!**
