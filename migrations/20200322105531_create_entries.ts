import * as Knex from "knex"

export async function up(knex: Knex): Promise<any> {
  return knex.schema.createTable('entries', table => {
    table.increments()
    table.string('email')
    table.string('code')
    table.boolean('sent')

    table.index('email')
    table.unique(['email'])
  })
}

export async function down(knex: Knex): Promise<any> {
  return knex.schema.dropTable('entries')
}
